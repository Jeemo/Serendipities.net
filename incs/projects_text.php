<?php

print <<<STOP

<h2 style='clear:left;'>Projects</h2>
  <dl>
    <dt><a href='http://kickball.com' target='_new' title="WAKA&copy; is the World Adult Kickball Association&copy; - the preeminent adult kickball organization and the world authority and governing body of kickball.">Kickball.com</a>
    </dt>
      <dd><strong>2008 - Present:</strong>  Drupal and custom modules, APIs, themes and user interface. PHP, MySQL, jQuery, Linux, SVN, Trac, PayflowPro Integration, Telecommuting. Site Development, design, functional improvements and company-wide tech support. WAKA Kickball serves over 150,000 players in 200+ leagues. I began at WAKA with two days Drupal experience. The new Drupal site launched on Feb. 15, 2009 with over 100,000 users ported from their old system. I was the only full-time programmer until April 2009. I currently share duties with two other full-time programmers.
      </dd>
  	<dt><a href='http://afreshwebdesignstlouis.com' target='_new' title="A Fresh Web Design is a full service web design, development and SEO company with it's headquarters in St. Louis, Missouri.">AfreshWebDesignStLouis.com</a>
  	</dt>
  	<dd><strong>2012 - 2014:</strong> Drupal 7 and its modules, APIs, themes and user interface. LAMP (Linux / Apache / 	MySQL / PHP), jQuery, AJAX/JSON, HTML/CSS. Senior Developer, Systems Administrator, Project 	Management, Mentor.  A Fresh Web Design is a one-stop design and development shop that typically 	has 8 – 10 projects working at any given time.  We cater to small and medium sized businesses with ecommerce business models. 	Every project is unique and requires some very creative leveraging of contributed Drupal 7 modules 	to provide the special services that make our clients stand out in their field.
  	</dd>
    <!-- dt><a href='http://kaiodee.com' target='_new' title="Drupal-based blog.  Excellent writing style.">Kaiodee.com</a>
    </dt>
      <dd><strong>2010 - Present:</strong> Drupal-based blog for a local college student.  She is looking forward to expanding the site quickly with Drupal.  This is part of a multi-site Drupal install setup that makes updates a snap.
      </dd -->
    <dt><a href='http://fredoniapubliclibrary.org' target='_new' title="Fredonia Public Library:  Your Window to the World.">FredoniaPublicLibrary.org</a>
    </dt>
      <dd><strong>2008 - Present:</strong> Drupal. Simple Drupal site for my local public library. Contributed modules only but allows the librarians to update content and provides a handy homepage for the library's public computers. This is setup as part of a multi-site Drupal install with a variety of clients and personal sites.
      </dd>
    <dt><a href='http://credibleartstherapies.org' target='_new' title="A simple, functional site for a very worthy cause.">CredibleArtsTherapies.org</a>
    </dt>
      <dd><strong>2008 - Present:</strong> Static HTML brochure-style site.  Effective and elegant.
      </dd>
    <dt><!-- a href='http://tedwatts.org' target='_new' title="PHP, dHTML. Developed to showcase the work of arguably the most prolific and well-known sports artist in America."-->TedWatts.org<!-- /a -->
    </dt>
      <dd><strong>2005 - Present:</strong> PHP, dHTML. Site implementation and maintenance. Developed to showcase the work of arguably the most prolific and well-known sports artist in America, this site is currently an extensive gallery with recent press releases and media coverage featured. The back-end directory structure is carefully organized to aid in updates and additions. E-Commerce functionality is in the planning stages at this time. 
      </dd>
    <dt><a href="http://centercreek.com" target='_new' title="Southeast Kansas Recording studio">Center Creek Records and Publishing</a>
    </dt>
      <dd><strong>2007 - Present</strong> PHP, dHTML.  Simple site that utilizes flatfiles instead of a database.
      </dd>
    <dt><a href='http://sme-portalsoftime.com' target='_new' title="PHP, dHTML. A very interesting topic made this project exciting. Currently, it is being redesigned and implemented using CakePHP.">SME-PortalsOfTime.com</a>
    </dt>
      <dd><strong>2002 - Present:</strong> PHP, dHTML. Site design, graphics, implementation and maintenance. A very interesting topic made this project exciting. It features many behind-the-scenes custom functions to make updates and expansion simple and quick. Eventually, we were invited to attend their Easter 'rendezvous'. WOW!
      </dd>
    <dt><!-- a href='http://cutestpetcontest.com' target='_new' title="PHP, MySQL. This high-traffic contest site handles hundreds of user uploads and form submissions daily with a simple interface that suits a wide demographic audience of international pet-lovers." -->CutestPetContest.com<!-- /a -->
    </dt>
      <dd><strong>2005 - 2010:</strong> PHP, MySQL. Site upgrade, design, development and maintenance with a legacy MySQL database. This high-traffic contest site handled hundreds of user uploads and form submissions daily with a simple interface that suited a wide demographic audience of international pet-lovers. Javascript was used for all form validation. This client knew exactly what he wanted and was very involved with every step of the process of building and maintaining the site. Numerous updates, improvements and specific marketing requirements were added over the years as requested. A simple, secure administration area was included for handling entries and tracking some basic statistics. 
      </dd>
    <dt><!-- a href='http://jocksnitch.com' target='_new' title="PHP, MySQL, OSCommerce. With four physical store locations in two states and the on-line store all utilizing and updating the OSCommerce inventory system daily, server stability is imperative for this well-established retailer." -->JocksNitch.com<!-- /a -->
    </dt>
      <dd><strong>2007 - 2009:</strong> PHP, MySQL, OSCommerce. Handled server administration, client support, OSCommerce functionality and site maintenance for this client. With four physical store locations in two states and the on-line store all utilizing and updating the OSCommerce inventory system constantly, server stability is imperative for this well-established retailer. The 2009 server migration and OSCommerce updates for the entire system were completed with less than two hours downtime over a couple of days for the on-line store. 
      </dd>
    <dt>Statepayments.com
    </dt>
      <dd><strong>2005 - 2008:</strong> PHP, MySQL. Site maintenance and support.  Provided government agencies such as county treasurers with a credit card payment option.  Server-side SaaS used the client browser and an inexpensive card swiper at the client end to provide a simple solution.
      </dd>
    <dt>ArtsCoffee.com & ArtisanRoasters.com
    </dt>
      <dd><strong>2002 - 2008:</strong>  Ruby on Rails, MySQL. Database and site development, implementation and maintenance as well as server and email administration. This boutique roasting house required a distinctive shopping experience combined with an exhaustively function-rich back-end that did everything except measure the coffee, roast it and package it for shipment. This left the client free to focus on customer service, their strongest attribute besides roasting coffee! The e-Commerce front end included customized credit card processing with the LinkPoint API, customer accounts, gifting options, multiple ship-to functions, periodic charges and pausing and canceling of recurring orders, and a dash of AJAX and Javascript for ease of use and an efficient user interface. It used SOAP and WSDL to interact with the US Postal Service API and the State of Kansas API for shipping and tax calculations. The back-end of the site handled, among other things, customer accounts, inventory, logistics, shipping, and promotional sales.
      </dd>  
    <dt>DracoDigital.com
    </dt>
      <dd><strong>2000 - 2008:</strong>  HTML, CSS, dHTML, PHP, Javascript, PostgreSQL.  Draco provided web development, design services and hosting to a variety of clients.
      </dd>   
  </dl>

STOP;

/*
 * <a name='current_projects' id='current_projects'></a>
<h2 style='clear:both;'>Current Projects</h2>
  <dl>
    <dt><a href='http://kickball.com' target='_new' title="WAKA&copy; is the World Adult Kickball Association&copy; - the preeminent adult kickball organization and the world authority and governing body of kickball.">Kickball.com</a></dt>
      <dd><strong>2008 ï¿½ Present:</strong>  Telecommuting, Drupal and its modules, APIs, themes and user interface.  PHP, MySQL, jQuery, Linux, SVN, Trac, PayflowPro Integration.  Site Development, design, functional improvements and company-wide tech support.  WAKA Kickball serves over 150,000 players in 200+ leagues. I began at WAKA with two days Drupal experience.  The new Drupal site launched on Feb. 15, 2009 with over 100,000 users ported from their old system.  I was the only full-time programmer until April 2009.  I currently share duties with one other full-time programmer. 
    <dt><a href='http://kaiodee.com' target='_new' title="Drupal-based blog.  Excellent writing style.">Kaiodee.com</a></dt>
      <dd><strong>2010 - Present:</strong> Drupal-based blog for a local college student.  She is looking forward to expanding the site quickly with Drupal.  This is part of a multi-site Drupal install setup that makes updates a snap.</dd>
    <dt><a href='http://fredoniapubliclibrary.org' target='_new' title="">FredoniaPublicLibrary.org</a></dt>
      <dd><strong>2008 - Present:</strong> Simple Drupal site for my local public library.  Contributed modules only but allows the librarians to update content and provides a handy homepage for the library's public computers.  This is setup as part of a multi-site Drupal install with a variety of clients and personal sites.</dd>
    <dt><a href='http://credibleartstherapies.org' target='_new' title="A simple, functional site for a very worthy cause.">CredibleArtsTherapies.org</a></dt>
      <dd><strong>2008 - Present:</strong> Static HTML brochure-style site.  Effective and elegant.</dd>
    <dt><a href='http://tedwatts.org' target='_new' title="PHP, dHTML. Developed to showcase the work of arguably the most prolific and well-known sports artist in America.">TedWatts.org</a></dt>
      <dd><strong>2005 - Present:</strong> PHP, dHTML.  Site implementation and maintenance.  Developed to showcase the work of arguably the most prolific and well-known sports artist in America, this site is currently an extensive gallery with recent press releases and media coverage featured.  The back-end directory structure is carefully organized to aid in updates and additions.  E-Commerce functionality is in the planning stages at this time.</dd>
    <dt><a href='http://sme-portalsoftime.com' target='_new' title="PHP, dHTML. A very interesting topic made this project exciting. Currently, it is being redesigned and implemented using CakePHP.">SME-PortalsOfTime.com</a></dt>
      <dd><strong>2002 - Present:</strong> PHP, dHTML.  Site design, graphics, implementation and maintenance.  A very interesting topic made this project exciting. It features many behind-the-scenes custom functions to make updates and expansion simple and quick. Eventually, we were invited to attend their Easter 'rendezvous'. WOW!  Currently, it is being redesigned and implemented using CakePHP.</dd>
    <dt><a href='http://cutestpetcontest.com' target='_new' title="PHP, MySQL. This high-traffic contest site handles hundreds of user uploads and form submissions daily with a simple interface that suits a wide demographic audience of international pet-lovers.">CutestPetContest.com</a></dt>
      <dd><strong>2005 - 2010:</strong> PHP, MySQL. Site upgrade, design, development and maintenance with a legacy MySQL database. This high-traffic contest site handles hundreds of user uploads and form submissions daily with a simple interface that suits a wide demographic audience of international pet-lovers. Javascript is used for all form validation.  This client knows exactly what he wants and is very involved with every step of the process of building and maintaining the site. Numerous updates, improvements and specific marketing requirements have been added over the years as requested. A simple, secure administration area is included for handling entries and tracking some basic statistics. </dd>
    <dt><a href='http://jocksnitch.com' target='_new' title="PHP, MySQL, OSCommerce. With four physical store locations in two states and the on-line store all utilizing and updating the OSCommerce inventory system daily, server stability is imperative for this well-established retailer.">JocksNitch.com</a></dt>
      <dd><strong>2007 - 2009:</strong> PHP, MySQL, OSCommerce. Currently handling server administration, client support, OSCommerce functionality and site maintenance for this client. With four physical store locations in two states and the on-line store all utilizing and updating the OSCommerce inventory system constantly, server stability is imperative for this well-established retailer. The recent server migration and OSCommerce updates for the entire system were completed with only sporadic downtime over a couple of days for the on-line store.</dd>
    <dt>Statepayments.com</dt>
      <dd><strong>2005 - 2008:</strong> PHP, MySQL, PayFlowPro. PHP, MySQL. Site maintenance and support.  Provided government agencies such as county treasurers with a credit card payment option.  Server-side SaaS used the client browser and an inexpensive card swiper at the client end to provide a simple solution.</dd>
    <dt>ArtsCoffee.com</dt>
      <dd><strong>2002 ï¿½ 2008:</strong>  Unfortunately, Artscoffee.com closed its doors in 2008.  Ruby on Rails, MySQL. Database and site development, implementation and maintenance as well as server and email administration. This boutique roasting house required a distinctive shopping experience combined with an exhaustively function-rich back-end that does everything except measure the coffee, roast it and package it for shipment. This leaves the client free to focus on customer service, their strongest attribute besides roasting coffee! Currently, the e-Commerce front end includes customized credit card processing with the LinkPoint API, customer accounts, gifting options, multiple ship-to functions, periodic charges and pausing and canceling of recurring orders, and a dash of AJAX and Javascript for ease of use and an efficient user interface. It uses SOAP and WSDL to interact with the US Postal Service API and the State of Kansas API for shipping and tax calculations. The back-end of the site handles, among other things, customer accounts, inventory, logistics, shipping, and promotional sales . Future functionality is currently scheduled to include shipping label generation, automatic inventory control, financial forecasting, extensive reporting options and marketing promotions.   I have been in charge of ArtsCoffee.com since 2002 except for 2006 when they chose to go with a different programmer.  Please note that they dropped that programmer and re-hired me two years ago.</dd>   
    <dt>ArtisanRoasters.com</dt>
      <dd><strong>2007 - 2008:</strong> Also closed its doors in 2008.  Ruby on Rails, MySQL.  Site design, implementation and maintenance.  Currently in use as an informational site.  Development is ongoing to transform it into a full-fledged business app and e-Commerce site similar to ArtsCoffee.com for the same company.</dd>      
  </dl>
<p class='basic'>
I look forward to discussing how I can help your endeavor reach its maximum potential with a customized web solution. I will be happy to provide code examples for serious business inquiries.
</p>
 */
?>