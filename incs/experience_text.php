<?php

print <<<STOP

<h2 style='clear:left;'>Experience</h2>
  <ul>
  <li>15 years experience handling ALL aspects of your website needs from inception to front-end development to ongoing maintenance.
  </li><li>Telecommute professionally and routinely for 15 years.
  </li><li>Hand-code all websites with text editor. 
  </li><li>Sites are validated with W3C in order to be compliant with assistive devices.
  </li>
  </ul>

<dl>
<dt>Dynamic Websites</dt>
  <dd>Design and implement dynamic database-driven sites that utilize PHP, SQL, HTML, HTML5, CSS, CSS3, XML, XHTML, dHTML, Javascript, jQuery, MySQL, PostgreSQL, AJAX. LAPP/LAMP-based. Especially e-Commerce.
  </dd>
<dt>e-Commerce</dt>
  <dd>Ubercart, OSCommerce, ZenCart and Custom systems.  Design, Setup, Integration, Maintenance.  Integration with service-added APIs such as Paypal, LinkPoint, PayFlowPro, US Postal Service, State of Kansas Tax Rates.
  <br />Experienced in porting data between eCommerce solutions.
  </dd>
<dt>Content Management Systems (CMS)</dt>
  <dd>Custom Content Management system design and implementation.
  </dd>
<dt>Drupal</dt>
  <dd>Performance, New Relic, Acquia, Contrib Modules, Custom Modules, UberCart, Views, Organic Groups, CCK, Multi-Site Installations.
  </dd>
<dt>Model/View/Controller Frameworks (MVC Frameworks)</dt>
  <dd>Ruby on Rails, CakePHP
  </dd>
<dt>API Integration</dt>
  <dd>Custom integration from API specifications.  Current experience includes YourPay, Linkpoint, PayFlowPro, PayPal, US Postal Service, State of Kansas Tax Rates.
  </dd>
<dt>Basic LAMP/LAPP Server Administration</dt>
  <dd>Linux, Apache, Mysql, PostgreSQL, PHP, OSCommerce, cPanel, Gmail, Google Apps and/or Plesk.
  </dd>
<dt>eBook Conversions</dt>
  <dd>Convert manuscripts (usually MS Word documents) to .epub, .mobi files for use with eBook readers.  Nook, Kindle, Sony, etc.
  </dd>
<dt>Software as a Service</dt>
  <dd>SaaS.  Custom system that allowed government agencies to accept credit card payments online.
  </dd>
<dt>Additonal Software and Development Tools:</dt>
  <dd>Git, Subversion (SVN), Trac, Agile Development, Regression Testing, Office, OpenOffice, GIMP, PaintShopPro, Photoshop
  </dd>
<dt>Graphic Design</dt>
  <dd>I am not a Graphic Designer.  I will gladly work with your graphic designer's files or I have connections to several outstanding and talented artists who would be interested in working with you directly.
  </dd>
</dl>

STOP;

/*
 * <ul>
<li>Design and implement database-driven sites that utilize SQL, especially e-Commerce. 11 years experience. 
</li><li>Typically handle ALL aspects of a client's website needs from inception to front-end development to ongoing maintenance. 
</li><li>Telecommute routinely (11 years experience, Broadband Internet access, webcam on request). 
</li><li>Hand-code all websites with text editor. 
</li><li>Custom Content Management system design and implementation. 11 years experience. 
</li><li>Proficient with XHTML, DHTML, and CSS. 
</li><li>PHP, MySQL, PostgreSQL.  11 years experience.
</li><li>Javascript. 10 years experience integrating JS with my work for additional functionality. 
</li><li>GIMP, PaintShopPro, Photoshop.  11 years experience creating sites from image files.
</li><li>API Integration. Currently includes YourPay, Linkpoint, PayFlowPro, PayPal, US Postal Service, State of Kansas Tax Rates, Drupal. 7 years experience. 
</li><li>Server Administration. Linux, OSCommerce, cPanel, and/or Plesk.  5 years experience. 
</li><li>Email Administration. Linux, cPanel, Plesk, Gmail, Google Apps. 5 years experience. 
</li><li>SaaS solutions. 4 years experience.
</li><li>Drupal, SVN, Trac. 18 months experience.
</li><li>Ruby on Rails. 2 years experience. 
</li><li>OSCommerce, ZenCart and experience porting from those e-commerce solutions to Drupal. 3 years.
</li><li>CakePHP, jQuery. < 1 year. 
</li>
</ul>

 */
?>