<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="author" content="Jee" />
<meta name="description" content="Providing working solutions to your website problems." />
<meta name="keywords" content="webapps, php, postgresql, mysql, ruby on rails, RoR, HTML, CSS, freelance, contractor, programmer" />

<?php $domain = "Serendipities.net"; ?>
  <title>
  <?php 
    echo ucfirst($page);
    echo ":: ";
    $letters = str_split($domain);
    foreach ($letters as $letter) echo $letter." ";
  ?>
  </title>
<link href="layout.css" media="screen" rel="Stylesheet" type="text/css" />
<link href="default.css" media="screen" rel="Stylesheet" type="text/css" />
<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>

<div id="header"></div>

<?php
if ($handle = opendir('imgs/sidebars')) :
    $input = array();
   while (false !== ($file = readdir($handle))) :
if ($file != "." && $file != "..") :
         $input[] = $file;
endif;
    endwhile;
    closedir($handle);
endif;

$rand_key = array_rand($input);
echo "<div id=\"content\" style='background: url(/imgs/sidebars/".$input[$rand_key].") no-repeat 100% 95px;'>";

require_once("menu_text.php");
?> 
  
<a href="http://serendipities.net" target="_self" title="Serendipities.net"><img id="logo" src='imgs/logo_use.png' alt="Serendipities.net Logo" /></a>
