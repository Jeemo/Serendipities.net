<?php

echo "
<p class='basic' style='clear:left;'>
Are you looking for a fast, sturdy site that combines dynamic pages with database capabilities and is EASY to update? With ".(date('Y') - 1998)." years experience in designing, implementing, and maintaining dynamic web sites that utilize MySQL and PostgreSQL, I can build a custom PHP or Drupal web application that fits your needs perfectly.
</p><p class='basic'>
Imagine an on-line store that presents your business personality perfectly to your clients and ties in seamlessly with a custom back-end that helps you serve your customers and business requirements logically and efficiently. Utilizing Web 2.0 strategies, your site will be standards-compliant, handicapped-accessible, mobile-accessible, fluid, elegant, and functional.
</p><p class='basic'>
Perhaps you simply need somebody to jump in and take over maintenance of your current website? I can handle your website administration needs quickly and efficiently. I know how frustrating it is when your emails and phone calls aren't answered in a timely manner and take pride in responding quickly to client requests. 
</p><p class='basic'>
I started handcoding websites professionally in 1998. You will find a <a href='projects.php'>number of sites listed</a> that represent my work and range from simple HTML to a customized web application that is designed to handle every aspect of a business that can be automated. These sites are in various stages of completion and include descriptions for your convenience.
</p><p class='basic'> 
Does the project you have in mind require a different programming language, skillset or established Content Management System?  I have a <a href='experience.php'>a lot of experience</a> already, but love a new challenge!  :) 
</p><p class='basic'>
I look forward to discussing how <a href='mailto:inquiries@".strtolower($domain)."?subject=Web Contact'>I can help your endeavor</a> reach its maximum potential with a customized web solution. I will be happy to provide code examples for serious business inquiries.
</p>
";
/*
 * <p class='basic'>
Are you looking for a fast, sturdy site that combines dynamic pages with database capabilities and is EASY to update?  With eleven years experience in designing, implementing, and maintaining dynamic web sites that utilize MySQL and PostgreSQL, I can build a custom web application that fits your needs perfectly.
</p><p class='basic'>
Imagine an on-line store that presents your business personality perfectly to your clients and ties in seamlessly with a custom back-end that helps you serve your customers and business requirements logically and efficiently. Utilizing Server-Side Includes(SSI), Cascading Style Sheets(CSS), and Javascript, your site will be standards-compliant, handicapped-accessible, fluid, elegant, and functional. 
</p><p class='basic'>
Perhaps you simply need somebody to jump in and take over maintenance of your current website?  I can handle your website administration needs quickly and efficiently.  I know how frustrating it is when your emails and phone calls aren't answered in a timely manner and take pride in responding quickly to my clients' requests.
</p><p class='basic'>
I started handcoding websites professionally in 1998. You will find a <a href='#current_projects'>number of sites listed below</a> that represent my work and range from simple HTML to a customized web application that is designed to handle every aspect of a business that can be automated. These sites are in various stages of completion and include descriptions for your convenience.
</p><p class='basic'>
As a highly experienced telecommuter, I know that being reliable, dependable, and productive is key to being successful.  I believe my employment history proves that I am all three.  For the last two years, I have been 50% of the tech team for a 100% virtual site with over 150 staff members and 50,000+ active users.  Our website and email servers are our primary communication tool both internally and externally.  Responding rapidly and effectively to the constantly evolving needs of such a system is absolutely critical to the viability of the company and I am proud to say that I have risen to the challenge.  
</p><p class='basic'>
Does the project you have in mind require a different programming language, skillset or established Content Management System?  I love a challenge.  :)
</p>

 */
?>